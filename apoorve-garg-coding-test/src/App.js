import React, { Component } from 'react';
import { BrowserRouter, Route, Redirect } from 'react-router-dom'
import './App.css';

import PassingDataContext from './Context/dataPassing';
import MainNavigation from './components/MainNavigation/MainNavigation';
import Tweets from './components/Tweets/Tweets';
import Profile from './components/Profile/Profile';

class App extends Component {

  state = {
    tweets: []
  }


  componentDidMount() {
    fetch(`https://www.tronalddump.io/tag`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      }
    }).then(res => {
      if (res.status !== 200 && res.status !== 201) {
        res.json().then(err => console.log(err.message))
        throw new Error(res)
      }
      return res.json()
    }).then(resData => {
      let a = []
      a.push(resData)
      this.setState({ tweets: a })
    })
  }

  render() {

    let load = <p className="loader">Loading...</p>
    if (this.state.tweets.length > 0) {
      load = (
        <PassingDataContext.Provider value={{ tweets: this.state.tweets }}>
          <MainNavigation />
          <main className="main-content">
            <Redirect from="/" to="/tweets" exact />
            <Route path="/tweets" component={Tweets} />
            <Route path="/profile" component={Profile} />
          </main>
        </PassingDataContext.Provider>
      )
    }
    return (
      <BrowserRouter>
        <React.Fragment>
          {load}
        </React.Fragment>
      </BrowserRouter>
    );
  }
}

export default App;
