import React, { useState, useEffect } from 'react'

import './Tweet.css'

const Tweet = (props) => {

    const [tweet, setTweet] = useState([])

    useEffect(() => {
        fetch(`https://www.tronalddump.io/search/quote?tag=${props.tag}`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(res => {
            if (res.status !== 200 && res.status !== 201) {
                res.json().then(err => console.log(err.message))
                throw new Error(res)
            }
            return res.json()
        }).then(resData => {
            let a = []
            a.push(resData)
            setTweet(a)
        })
    }, [props.tag])
    let loadTweet = <p className="loader">Loading...</p>
    if (tweet.length > 0) {
        loadTweet = (
            tweet[0]._embedded.quotes.map((twe, index) => (
                <div className="tweet-value" key={index}>
                    <p>"{twe.value}"</p>
                </div>
            ))
        )
    }
    return (
        <div className="tweet-header">
            {loadTweet}
        </div>
    )
}

export default Tweet