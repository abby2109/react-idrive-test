import React from 'react'
import Logo from '../../assets/Trump4.jpg';

import './Profile.css'

const profile = (props) => {

    const seeTweetsHandler = () => {
        props.history.push('/tweets')
    }

    return (
        <div className="image-profile">
            <div className="image-header">
                <img src={Logo} className="img" alt="Donald Trump" />
            </div>
            <div>
                <h2>Donald J. Trump</h2>
                <p>President of United States</p>
                <p>14 June 1946</p>
                <p onClick={seeTweetsHandler} className="tweet">See Tweets</p>
            </div>
        </div>
    )
}

export default profile