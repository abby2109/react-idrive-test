import React from 'react'
import { NavLink } from 'react-router-dom'

import './MainNavigation.css'

const mainNavigation = (props) => {
    return (
        <header className="main-navigation">
            <div className="main-navigation-title">
                <h1>Donald Trump Tweet</h1>
            </div>
            <nav className="main-navigation-items">
                <ul>
                    <React.Fragment>
                        <li><NavLink to="/tweets">Tweets</NavLink></li>
                        <li><NavLink to="/profile">Profile</NavLink></li>
                    </React.Fragment>
                </ul>
            </nav>
        </header>
    )
}

export default mainNavigation